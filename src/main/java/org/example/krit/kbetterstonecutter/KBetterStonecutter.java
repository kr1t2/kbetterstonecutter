package org.example.krit.kbetterstonecutter;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.StonecuttingRecipe;
import org.bukkit.plugin.java.JavaPlugin;

public final class KBetterStonecutter extends JavaPlugin {

    private static KBetterStonecutter plugin;

    @Override
    public void onEnable() {

        plugin = this;

        addWoodCraft(Material.OAK_LOG);
        addWoodCraft(Material.DARK_OAK_LOG);
        addWoodCraft(Material.BIRCH_LOG);
        addWoodCraft(Material.ACACIA_LOG);
        addWoodCraft(Material.SPRUCE_LOG);
        addWoodCraft(Material.CHERRY_LOG);
        addWoodCraft(Material.JUNGLE_LOG);
        addWoodCraft(Material.MANGROVE_LOG);

        addWoodCraft(Material.WARPED_STEM);
        addWoodCraft(Material.CRIMSON_STEM);

    }

    private void addWoodCraft(Material wood) {

        boolean isNetherWood = wood.toString().endsWith("STEM");

        // Древесина обтёсанная
        ItemStack strippedLog = new ItemStack(Material.valueOf("STRIPPED_" + wood.toString()));
        Material wood2 = Material.valueOf(wood.toString().substring(0, isNetherWood ? wood.toString().length() - 4 : wood.toString().length() - 3) + (isNetherWood ? "HYPHAE" : "WOOD"));
        ItemStack strippedWood = new ItemStack(Material.valueOf("STRIPPED_" + wood.toString().substring(0, isNetherWood ? wood.toString().length() - 4 : wood.toString().length() - 3) + (isNetherWood ? "HYPHAE" : "WOOD")));
        Bukkit.getServer().addRecipe(new StonecuttingRecipe(new NamespacedKey(getInstance(), wood.toString() + "_KBETTER_STONECUTTER"), strippedLog , wood));
        Bukkit.getServer().addRecipe(new StonecuttingRecipe(new NamespacedKey(getInstance(), wood.toString() + "_FROMWOOD_KBETTER_STONECUTTER"), strippedLog , wood2));
        Bukkit.getServer().addRecipe(new StonecuttingRecipe(new NamespacedKey(getInstance(), wood.toString() + "_FROMSTRIPPEDWOOD_KBETTER_STONECUTTER"), strippedLog , strippedWood.getType()));

        // Древесина всесторонняя обтёсанная
        Bukkit.getServer().addRecipe(new StonecuttingRecipe(new NamespacedKey(getInstance(), wood.toString().substring(0, (isNetherWood ? wood.toString().length() - 4 : wood.toString().length() - 3)) + (isNetherWood ? "HYPHAE" : "WOOD") + "_KBETTER_STONECUTTER"), strippedWood , wood2));
        Bukkit.getServer().addRecipe(new StonecuttingRecipe(new NamespacedKey(getInstance(), wood.toString().substring(0, (isNetherWood ? wood.toString().length() - 4 : wood.toString().length() - 3)) + (isNetherWood ? "HYPHAE" : "WOOD") + "_FROMWOOD_KBETTER_STONECUTTER"), strippedWood , wood));
        Bukkit.getServer().addRecipe(new StonecuttingRecipe(new NamespacedKey(getInstance(), wood.toString().substring(0, (isNetherWood ? wood.toString().length() - 4 : wood.toString().length() - 3)) + (isNetherWood ? "HYPHAE" : "WOOD") + "_FROMSTRIPPEDLOG_KBETTER_STONECUTTER"), strippedWood , strippedLog.getType()));


        // Доски
        ItemStack planks = new ItemStack(Material.valueOf(wood.toString().substring(0, isNetherWood ? wood.toString().length() - 4 : wood.toString().length() - 3) + "PLANKS"));
        planks.setAmount(4);
        Bukkit.getServer().addRecipe(new StonecuttingRecipe(new NamespacedKey(getInstance(), wood.toString().substring(0, isNetherWood ? wood.toString().length() - 4 : wood.toString().length() - 3) + "PLANKS_KBETTER_STONECUTTER"), planks, wood));
        Bukkit.getServer().addRecipe(new StonecuttingRecipe(new NamespacedKey(getInstance(), wood.toString().substring(0, isNetherWood ? wood.toString().length() - 4 : wood.toString().length() - 3) + "PLANKS_FROMWOOD_KBETTER_STONECUTTER"), planks, wood2));
        Bukkit.getServer().addRecipe(new StonecuttingRecipe(new NamespacedKey(getInstance(), wood.toString().substring(0, isNetherWood ? wood.toString().length() - 4 : wood.toString().length() - 3) + "PLANKS_FROMSTRIPPEDWOOD_KBETTER_STONECUTTER"), planks, strippedWood.getType()));
        Bukkit.getServer().addRecipe(new StonecuttingRecipe(new NamespacedKey(getInstance(), wood.toString().substring(0, isNetherWood ? wood.toString().length() - 4 : wood.toString().length() - 3) + "PLANKS_FROMSTRIPPEDLOG_KBETTER_STONECUTTER"), planks, strippedLog.getType()));

        // Ступени
        ItemStack stairs = new ItemStack(Material.valueOf(wood.toString().substring(0, isNetherWood ? wood.toString().length() - 4 : wood.toString().length() - 3) + "STAIRS"));
        stairs.setAmount(4);
        Bukkit.getServer().addRecipe(new StonecuttingRecipe(new NamespacedKey(getInstance(), wood.toString().substring(0, isNetherWood ? wood.toString().length() - 4 : wood.toString().length() - 3) + "STAIRS_KBETTER_STONECUTTER"), stairs, wood));
        Bukkit.getServer().addRecipe(new StonecuttingRecipe(new NamespacedKey(getInstance(), wood.toString().substring(0, isNetherWood ? wood.toString().length() - 4 : wood.toString().length() - 3) + "STAIRS_FROMWOOD_KBETTER_STONECUTTER"), stairs, wood2));
        Bukkit.getServer().addRecipe(new StonecuttingRecipe(new NamespacedKey(getInstance(), wood.toString().substring(0, isNetherWood ? wood.toString().length() - 4 : wood.toString().length() - 3) + "STAIRS_FROMSTRIPPEDWOOD_KBETTER_STONECUTTER"), stairs, strippedWood.getType()));
        Bukkit.getServer().addRecipe(new StonecuttingRecipe(new NamespacedKey(getInstance(), wood.toString().substring(0, isNetherWood ? wood.toString().length() - 4 : wood.toString().length() - 3) + "STAIRS_FROMSTRIPPEDLOG_KBETTER_STONECUTTER"), stairs, strippedLog.getType()));
        stairs.setAmount(1);
        Bukkit.getServer().addRecipe(new StonecuttingRecipe(new NamespacedKey(getInstance(), wood.toString().substring(0, isNetherWood ? wood.toString().length() - 4 : wood.toString().length() - 3) + "STAIRS_FROMPLANKS_KBETTER_STONECUTTER"), stairs, planks.getType()));

        // Плитки
        ItemStack slabs = new ItemStack(Material.valueOf(wood.toString().substring(0, isNetherWood ? wood.toString().length() - 4 : wood.toString().length() - 3) + "SLAB"));
        slabs.setAmount(12);
        Bukkit.getServer().addRecipe(new StonecuttingRecipe(new NamespacedKey(getInstance(), wood.toString().substring(0, isNetherWood ? wood.toString().length() - 4 : wood.toString().length() - 3) + "SLAB_KBETTER_STONECUTTER"), slabs, wood));
        Bukkit.getServer().addRecipe(new StonecuttingRecipe(new NamespacedKey(getInstance(), wood.toString().substring(0, isNetherWood ? wood.toString().length() - 4 : wood.toString().length() - 3) + "SLAB_FROMWOOD2_KBETTER_STONECUTTER"), slabs, wood2));
        Bukkit.getServer().addRecipe(new StonecuttingRecipe(new NamespacedKey(getInstance(), wood.toString().substring(0, isNetherWood ? wood.toString().length() - 4 : wood.toString().length() - 3) + "SLAB_FROMWOOD_KBETTER_STONECUTTER"), slabs, strippedWood.getType()));
        Bukkit.getServer().addRecipe(new StonecuttingRecipe(new NamespacedKey(getInstance(), wood.toString().substring(0, isNetherWood ? wood.toString().length() - 4 : wood.toString().length() - 3) + "SLAB_FROMLOG_KBETTER_STONECUTTER"), slabs, strippedLog.getType()));
        slabs.setAmount(3);
        Bukkit.getServer().addRecipe(new StonecuttingRecipe(new NamespacedKey(getInstance(), wood.toString().substring(0, isNetherWood ? wood.toString().length() - 4 : wood.toString().length() - 3) + "SLAB_FROMPLANKS_KBETTER_STONECUTTER"), slabs, planks.getType()));




    }

    private static KBetterStonecutter getInstance() {
        return plugin;
    }
}
